//
//  MoviesViewController.swift
//  TMDb_Test
//
//  Created by Vladimirus on 03/08/2019.
//  Copyright © 2019 Vladimirus. All rights reserved.
//

import UIKit


class MoviesViewController: UIViewController {

    @IBOutlet weak var collectionViewData: UICollectionView!
    @IBOutlet weak var waitingAnimationView: UIView!
    
    private let cellIdentifier = "MovieCollectionViewCell"
    private let segueIdentifier = "toDetailVCSegue"
    
    private var movies = [Movie]()
    lazy var favouritesMovies = [Favourites]()
    
    private var pageNumber: Int = 0
    private var totalPageCount = Int()
    private var isFavouritesMoviesPage: Bool = false
    private var vcState: MoviesVCState = .empty {
        didSet {
            changeUI(for: vcState)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerXibs()
        DispatchQueue.main.async {
            self.fetchData(page: self.pageNumber + 1)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchFromDB()
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionViewData.collectionViewLayout.invalidateLayout()
    }
    
    func registerXibs() {
        collectionViewData.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
    }
    
    
    func changeUI(for state: MoviesVCState) {
        switch state {
        case .error:
            self.notificationAlert(status: .error)
        case .finished:
            hideNotification()
            waitingAnimationView.isHidden = true
        case .notInternetConnection:
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.fetchData(page: self.pageNumber + 1)
                self.notificationAlert(status: .notInternetConnection, isDisplayNow: true)
            }
        case .movieLoading:
            waitingAnimationView.isHidden = false
        default:
            break
        }
    }
    
    
    func fetchData(page: Int) {
        guard Reachability.isConnectedToNetwork() else {
            vcState = .notInternetConnection
            return
        }
        vcState = .loading
        NetworkService.getPopularMovies(page: page) { [weak self] (data, statusCode, error) in
            guard let self = self else { return }
            self.vcState = .finished
            guard let data = data,
                statusCode == 200,
                error == nil,
                let popularMovie = try? JSONDecoder().decode(PopularMovie.self, from: data) else {
                    self.vcState = .error
                    return
            }
            self.pageNumber += 1
            self.totalPageCount = popularMovie.totalPages ?? 1
            let _movies = popularMovie.results ?? []
            self.movies += _movies
            self.reloadCollectionItems(totalCount: self.movies.count, fetchCount: _movies.count)
        }
    }
    
    
    func fetchFromDB() {
        if let favourites = CoreDataManager.shared.fetchAllPersons() {
            self.favouritesMovies = favourites
        }
    }
    
    
    func reloadCollectionItems(totalCount: Int, fetchCount: Int) {
        guard fetchCount >= 0 else { return }
        var ips = [IndexPath]()
        let startIndex = totalCount - fetchCount
        for row in startIndex..<totalCount {
            ips.append(IndexPath(row: row, section: 0))
        }
        collectionViewData.insertItems(at: ips)
    }
    
    
    func isMovieFavourite(id: Int?) -> Favourites? {
        let filtered = self.favouritesMovies.filter() {Int($0.id) == id}.first
        return filtered
    }
 

    @IBAction func segmentControlPressed(_ sender: UISegmentedControl) {
        isFavouritesMoviesPage = !isFavouritesMoviesPage
        collectionViewData.reloadData()
    }
    
}



extension MoviesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return isFavouritesMoviesPage ? favouritesMovies.count : movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = collectionViewData.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! MovieCollectionViewCell
        if isFavouritesMoviesPage {
            let favourite = favouritesMovies[indexPath.row]
            item.updateItemData(for: favourite)
        } else {
            let movie = movies[indexPath.row]
            item.updateItemData(for: movie, isFavourite: isMovieFavourite(id: movie.id) != nil)
        }
        
        item.delegate = self
        return item
    }
    
    
}


extension MoviesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        vcState = .movieLoading
        NetworkService.getVideo(urlString: isFavouritesMoviesPage ? favouritesMovies[indexPath.row].trailerURL : movies[indexPath.row].trailerURL ?? "") { [weak self] (data, statusCode, error) in
            guard let self = self else { return }
            self.vcState = .finished
            guard let data = data,
                statusCode == 200,
                error == nil,
            let trailersInfo = try? JSONDecoder().decode(TrailersInfo.self, from: data),
            let key = trailersInfo.results?.first?.key else {
                    self.vcState = .error
                    return
            }
            self.presentVideoFor(key)
        }
 
    }
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height * 2) > scrollView.contentSize.height ) && vcState != .loading && vcState != .notInternetConnection && !isFavouritesMoviesPage) {
            DispatchQueue.main.async {
                let page = self.pageNumber + 1
                self.fetchData(page: page)
            }
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && vcState != .loading && vcState != .notInternetConnection && !isFavouritesMoviesPage) {
            scrollView.setContentOffset(scrollView.contentOffset, animated: false)
        }
    }
}


extension MoviesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let margins: CGFloat = 12
        let cellsInLine: CGFloat = (UIDevice.current.orientation.isLandscape ? 4 : 2)
        var width: CGFloat {
            return (collectionView.frame.width - (margins * (cellsInLine + 1))) / cellsInLine
        }
        let height = width * 1.5
        return CGSize(width: width, height: height)
    }
}



extension MoviesViewController: MovieCollectionViewCellDelegate {
    func addToFavourites(_ item: UICollectionViewCell) {
        guard let ip = collectionViewData.indexPath(for: item) else { return }
        if isFavouritesMoviesPage {
            let favouriteMovie = favouritesMovies[ip.row]
            CoreDataManager.shared.delete(favouriteMovie)
            fetchFromDB()
            collectionViewData.deleteItems(at: [ip])
        } else {
            let movie = movies[ip.row]
            if let filtered = isMovieFavourite(id: movie.id) {
                CoreDataManager.shared.delete(filtered)
                fetchFromDB()
                (item as? MovieCollectionViewCell)?.isFavouriteMovie = false
            } else {
                CoreDataManager.shared.insertFavourites(movie: movie)
                (item as? MovieCollectionViewCell)?.isFavouriteMovie = true
                fetchFromDB()
            }
        }
    }
    
}



