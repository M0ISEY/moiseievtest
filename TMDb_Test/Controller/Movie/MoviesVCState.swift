//
//  MoviesVCState.swift
//  TMDb_Test
//
//  Created by Vladimirus on 03/08/2019.
//  Copyright © 2019 Vladimirus. All rights reserved.
//

import Foundation

enum MoviesVCState {
    case loading
    case movieLoading
    case finished
    case empty
    case notInternetConnection
    case error
}
