//
//  MoviesVC+Extension.swift
//  TMDb_Test
//
//  Created by Vladimirus on 03/08/2019.
//  Copyright © 2019 Vladimirus. All rights reserved.
//

import XCDYouTubeKit
import AVKit
import SwiftEntryKit

extension MoviesViewController {
    func presentVideoFor(_ key: String) {
        let playerViewController = AVPlayerViewController()
        self.present(playerViewController, animated: true, completion: nil)
        XCDYouTubeClient.default().getVideoWithIdentifier(key) {
            [weak playerViewController] (video: XCDYouTubeVideo?, error: Error?) in
            if let streamURLs = video?.streamURLs, let streamURL =
                (streamURLs[XCDYouTubeVideoQuality.HD720.rawValue] ??
                    streamURLs[XCDYouTubeVideoQuality.medium360.rawValue] ??
                    streamURLs[XCDYouTubeVideoQuality.small240.rawValue]) {
                playerViewController?.player = AVPlayer(url: streamURL)
                playerViewController?.player?.play()
            } else {
                playerViewController?.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
    func notificationAlert(status: MoviesVCState, isDisplayNow: Bool = false) {
        var color: UIColor!
        var text = ""
        var duration: TimeInterval!
        
        switch status {
        case .notInternetConnection:
            color = .red
            text = "No internet connection"
            duration = .infinity
        case .error:
            color = .red
            text = "Error"
            duration = 1
        default:
            color = #colorLiteral(red: 0, green: 0.8269683719, blue: 0.4790723324, alpha: 1)
            text = "Done"
            duration = 1
        }
        
        let style = EKProperty.LabelStyle(font: UIFont.systemFont(ofSize: 16), color: .white, alignment: .center)
        
        let labelContent = EKProperty.LabelContent(text: text, style: style)
        let contentView = EKNoteMessageView(with: labelContent)
        
        var attributes = EKAttributes.bottomNote
        attributes.entryBackground = .color(color: EKColor(color))
        attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.7, duration: 1)))
        attributes.displayDuration = duration
        
        attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 10, offset: .zero))
        attributes.scroll = .enabled(swipeable: false, pullbackAnimation: .jolt)
        
        attributes.positionConstraints.size.height = EKAttributes.PositionConstraints.Edge.constant(value: 35)
        
        if isDisplayNow, SwiftEntryKit.isCurrentlyDisplaying {
            return
        }
        
        SwiftEntryKit.display(entry: contentView, using: attributes)
        
    }
    
    func hideNotification() {
        if SwiftEntryKit.isCurrentlyDisplaying() {
            SwiftEntryKit.dismiss()
        }
    }
    
    
}
