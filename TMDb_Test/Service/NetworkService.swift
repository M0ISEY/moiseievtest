//
//  NetworkService.swift
//  TMDb_Test
//
//  Created by Vladimirus on 03/08/2019.
//  Copyright © 2019 Vladimirus. All rights reserved.
//

import UIKit
import Alamofire

class NetworkService {
    
    static func getPopularMovies(page: Int, completion: @escaping (Data?, Int?, Error?) -> Void) {
        let url = URL(string: Config.popularUrl)
        
        let params: Parameters = [
            "api_key": Config.movieApiKey,
            "language": "en-US",
            "page": "\(page)"
        ]
        
        Alamofire.request(url!, method: .get, parameters: params, encoding: URLEncoding(destination: .queryString), headers: nil).responseJSON { response in
            completion(response.data, response.response?.statusCode, response.error)
        }
        
    }
    
    static func getVideo(urlString: String, completion: @escaping (Data?, Int?, Error?) -> Void) {
        
        let params: Parameters = [
            "api_key": Config.movieApiKey,
            "language": "en-US"
        ]
        
        Alamofire.request(URL(string: urlString)!, method: .get, parameters: params, encoding: URLEncoding(destination: .queryString), headers: nil).responseJSON { response in
            completion(response.data, response.response?.statusCode, response.error)
        }
        
    }
}
