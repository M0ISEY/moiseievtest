//
//  Favourites+CoreDataProperties.swift
//  TMDb_Test
//
//  Created by Vladimirus on 04/08/2019.
//  Copyright © 2019 Vladimirus. All rights reserved.
//
//

import Foundation
import CoreData


extension Favourites {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Favourites> {
        return NSFetchRequest<Favourites>(entityName: "Favourites")
    }

    @NSManaged public var titleText: String?
    @NSManaged public var imageUrl: String?
    @NSManaged public var id: Int64
    
    var trailerURL: String {
        return "https://api.themoviedb.org/3/movie/\(id)/videos"
    }

}
