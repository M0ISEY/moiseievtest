//
//  Config.swift
//  TMDb_Test
//
//  Created by Vladimirus on 03/08/2019.
//  Copyright © 2019 Vladimirus. All rights reserved.
//

import Foundation


struct Config {
    static let movieApiKey = "101ed412c5e6b7e90dcfeb851b20cde1"
    static let popularUrl = "https://api.themoviedb.org/3/movie/popular"
}
