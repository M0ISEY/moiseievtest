//
//  MovieCollectionViewCell.swift
//  TMDb_Test
//
//  Created by Vladimirus on 03/08/2019.
//  Copyright © 2019 Vladimirus. All rights reserved.
//

import UIKit
import Kingfisher

protocol MovieCollectionViewCellDelegate: class {
    func addToFavourites(_ item: UICollectionViewCell)
}


class MovieCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var favouriteButton: UIButton!

    weak var delegate: MovieCollectionViewCellDelegate?
    
    var isFavouriteMovie: Bool! {
        didSet {
            favouriteButton.alpha = isFavouriteMovie ? 1 : 0.5
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 10
    }
    
    func updateItemData(for movie: Movie, isFavourite: Bool) {
        isFavouriteMovie = isFavourite
        if let posterUrlString = movie.posterUrlString, let url = URL(string: posterUrlString) {
            movieImage.kf.setImage(with: url, options: [.transition(.fade(0.2))])
        }
        movieTitle.text = movie.title
    }
    
    
    func updateItemData(for favourite: Favourites) {
        isFavouriteMovie = true
        if let posterUrlString = favourite.imageUrl, let url = URL(string: posterUrlString) {
            movieImage.kf.setImage(with: url, options: [.transition(.fade(0.2))])
        }
        movieTitle.text = favourite.titleText
    }
    

    @IBAction func addToFavouritesPressed(_ sender: Any) {
        isFavouriteMovie = !isFavouriteMovie
        delegate?.addToFavourites(self)
    }
    
}

