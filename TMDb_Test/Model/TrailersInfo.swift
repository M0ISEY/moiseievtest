//
//  TrailerInfo.swift
//  TMDb_Test
//
//  Created by Vladimirus on 04/08/2019.
//  Copyright © 2019 Vladimirus. All rights reserved.
//

import Foundation

// MARK: - TrailerRequest
struct TrailersInfo: Codable {
    let id: Int?
    let results: [Trailer]?
}

// MARK: - Result
struct Trailer: Codable {
    let id, iso639_1, iso3166_1, key: String?
    let name, site: String?
    let size: Int?
    let type: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case iso639_1 = "iso_639_1"
        case iso3166_1 = "iso_3166_1"
        case key, name, site, size, type
    }
    
    
    var videoURL: String? {
        if let key = key {
            return "https://www.youtube.com/watch?v=\(key)"
        }
        return nil
    }
}
