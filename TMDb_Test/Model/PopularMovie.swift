//
//  PopularMovie.swift
//  TMDb_Test
//
//  Created by Vladimirus on 03/08/2019.
//  Copyright © 2019 Vladimirus. All rights reserved.
//

import Foundation

struct PopularMovie: Codable {
    let page, totalResults, totalPages: Int?
    let results: [Movie]?
    
    enum CodingKeys: String, CodingKey {
        case page
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case results
    }
}
